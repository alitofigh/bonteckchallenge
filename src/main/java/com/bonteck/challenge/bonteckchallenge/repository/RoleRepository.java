package com.bonteck.challenge.bonteckchallenge.repository;

import com.bonteck.challenge.bonteckchallenge.model.RoleEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Ali Tofigh 2/9/2022 5:48 PM
 */

public interface RoleRepository extends CrudRepository<RoleEntity, Long> {
}
