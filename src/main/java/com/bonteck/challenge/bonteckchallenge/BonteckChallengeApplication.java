package com.bonteck.challenge.bonteckchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class BonteckChallengeApplication {

    public static void main(String[] args) {
        SpringApplication.run(BonteckChallengeApplication.class, args);
    }

}
